variable "name" {
    type    = string
    default = "qa1"
}

variable "image" {
    type    = string
}

variable "port" {
    type    = number
    default = 8080
}

variable "instance_cores" {
    type    = number
    default = 2
}

variable "instance_memory" {
    type    = number
    default = 4
}

variable "platform" {
    type = string
    default = "standard-v1"
}

locals {
    env = [
    ]
}

variable "folder_id" {
    type    = string
    default = ""
}

variable "zone" {
    type    = string
    default = "ru-central1-a"
}

variable "docker_logs" {
  type    = bool
  default = true
}

variable "extended_metrics" {
  type    = bool
  default = true
}

variable "alb_logs" {
  type    = bool
  default = false
}

variable "retention_period" {
  type    = number
  default = 2
}

variable "cidr_block" {
    type    = string
    default = "10.0.0.0/16"
}
